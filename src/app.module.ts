import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Patient } from './patients/entities/patient.entity';
import { PatientsModule } from './patients/patients.module';

// ES7 Decorators
@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'sqlite',
      host: join(__dirname, 'database.sqlite'),
      autoLoadModels: true,
      models: [Patient],
    }),
    PatientsModule,
  ], // Registrar outros modulos
  controllers: [AppController], // Registrar Controlador (Recebe a requisição e devolve a resposta)
  providers: [AppService],
})
export class AppModule {}
