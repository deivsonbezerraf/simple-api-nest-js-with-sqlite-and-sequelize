import { IsNotEmpty, IsNumber, IsPositive } from 'class-validator';

export class CreatePatientDto {
  @IsNotEmpty()
  patient_name: string;
}
