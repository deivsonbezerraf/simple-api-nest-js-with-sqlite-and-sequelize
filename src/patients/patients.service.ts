import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreatePatientDto } from './dto/create-patient.dto';
import { UpdatePatientDto } from './dto/update-patient.dto';
import { Patient } from './entities/patient.entity';

@Injectable()
export class PatientsService {
  constructor(
    @InjectModel(Patient)
    private patientsModel: typeof Patient,
  ) {}

  create(createPatientDto: CreatePatientDto) {
    return this.patientsModel.create({
      patient_name: createPatientDto.patient_name,
    });
  }

  findAll() {
    return this.patientsModel.findAll();
  }

  findOne(id: string) {
    return this.patientsModel.findByPk(id);
  }

  async update(id: string, updatePatientDto: UpdatePatientDto) {
    const patient = await this.patientsModel.findByPk(id);
    patient.update({
      patient_name: updatePatientDto.patient_name,
    });
    return patient;
  }

  async remove(id: string) {
    const patient = await this.patientsModel.findByPk(id);
    patient.destroy();
  }
}
