import {
  Column,
  DataType,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';

@Table({
  tableName: 'patients',
})
export class Patient extends Model {
  @PrimaryKey
  @Column({ type: DataType.UUID, defaultValue: DataType.UUIDV4 })
  patient_id: string;

  @Column({ allowNull: false, type: DataType.TEXT })
  patient_name: string;
}
