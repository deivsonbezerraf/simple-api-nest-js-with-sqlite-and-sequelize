import { Injectable } from '@nestjs/common';

// Pode ser injetado em outros módulos
@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}

// Regras de negócios
// Utils
