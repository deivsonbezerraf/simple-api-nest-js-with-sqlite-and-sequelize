import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('/users/v1')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('user')
  getNome(): string {
    return 'Bem-vindo Deivson';
  }
}
